import asyncio
import os
import unittest
from time import time

from aiounittest import AsyncTestCase

from mavtel_client import MAVTelClient
from mavtel_models import get_supported_metrics, MAVLinkMetricBaseModel

METRIC_UPDATES_PERIOD = 1.
ALL_METRIC_UPDATES_PERIOD = 5.


class FunctionalBasicsCase(AsyncTestCase):
    def setUp(self) -> None:
        self.url = os.environ.get('MAVTEL_URL', 'http://0.0.0.0:8000')
        self.client = MAVTelClient(self.url)

    async def test_mavlink_server_is_present(self):
        healthcheck_status = await self.client.healthcheck()
        self.assertTrue(healthcheck_status, f'MAVTel is missing! URL: {self.url}.')

    async def test_metric_updates(self):
        start = time()
        received_metric = False

        async for update in self.client.metric_updates():
            received_metric = True
            self.assertTrue(isinstance(update, MAVLinkMetricBaseModel), f'Incorrect metric class: "{type(update)}"')
            if time() >= start + METRIC_UPDATES_PERIOD:
                break

        self.assertTrue(received_metric, 'No metrics were received.')

    async def test_specific_metric_updates(self):
        all_metrics = set(get_supported_metrics().keys())
        metrics_to_test = all_metrics.difference({
            'raw_imu',
            'rc_status',
            'actuator_output_status',
            'ground_truth',
        })
        metrics_receive_statuses = {
            metric_name: False for metric_name in metrics_to_test
        }
        metric_tasks = {}

        async def _get_metric(metric_name: str):
            metric_class = get_supported_metrics()[metric_name]

            async for update in self.client.metric_updates({metric_name}):
                metrics_receive_statuses[metric_name] = True
                self.assertTrue(isinstance(update, metric_class), f'Incorrect metric class: "{type(update)}"')

        for metric_name in metrics_to_test:
            metric_tasks[metric_name] = asyncio.create_task(_get_metric(metric_name))

        # Just in case we will get all the metrics earlier than `ALL_METRIC_UPDATES_PERIOD`
        for i in range(10):
            await asyncio.sleep(ALL_METRIC_UPDATES_PERIOD / 10)
            missing_metrics = {k for k, v in metrics_receive_statuses.items() if not v}
            if len(missing_metrics) == 0:
                break

        for task in metric_tasks.values():
            task.cancel()

        missing_metrics = {k for k, v in metrics_receive_statuses.items() if not v}
        self.assertTrue(len(missing_metrics) == 0, f'Missing metrics: {", ".join(missing_metrics)}')

    async def test_retrieve_metrics(self):
        metrics_health = dict()

        for metric_name, metric_class in get_supported_metrics().items():
            try:
                metric = await getattr(self.client, metric_name)()
            except Exception as err:
                raise err

            is_healthy = metric is None or isinstance(metric, metric_class)
            metrics_health[metric_name] = is_healthy

        for metric_name, is_healthy in metrics_health.items():
            metric_class = get_supported_metrics()[metric_name]
            self.assertTrue(
                is_healthy,
                f'Invalid response for metric "{metric_name}", expected instance of {metric_class.__name__}'
            )


if __name__ == '__main__':
    unittest.main()
