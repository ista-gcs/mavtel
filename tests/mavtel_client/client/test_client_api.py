import unittest

from mavtel_client import MAVTelClient
from mavtel_models import get_supported_metrics


class MAVTelClientAPITestCase(unittest.TestCase):
    def test_health_check_available(self):
        client: MAVTelClient = MAVTelClient('http://0.0.0.0:8000')
        self.assertTrue(
            hasattr(client, 'healthcheck'),
            f'Health check is missing in {client.__class__.__name__}'
        )

    def test_metric_updates_available(self):
        client: MAVTelClient = MAVTelClient('http://0.0.0.0:8000')
        self.assertTrue(
            hasattr(client, 'metric_updates'),
            f'Metric updates is missing in {client.__class__.__name__}'
        )

    def test_metrics_mixin_installed(self):
        client: MAVTelClient = MAVTelClient('http://0.0.0.0:8000')
        for metric_name in get_supported_metrics().keys():
            self.assertTrue(
                hasattr(client, metric_name),
                f'Metric "{metric_name}" is missing in {client.__class__.__name__}'
            )

    def test_services_mixin_installed(self):
        client: MAVTelClient = MAVTelClient('http://0.0.0.0:8000')
        for service_name in ['collector', 'session', 'storage']:
            self.assertTrue(
                hasattr(client, service_name),
                f'Service "{service_name}" is missing in {client.__class__.__name__}'
            )


if __name__ == '__main__':
    unittest.main()
