import logging

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_utils.timing import add_timing_middleware

from mavtel.api.router import router
from mavtel.lifecycle import setup_services
from mavtel.logging import setup_logging
from mavtel.settings import settings

setup_logging()

app_params = dict(
    docs_url=settings.SWAGGER_DOCS_URL,
    redoc_url=settings.REDOC_DOCS_URL,
)

app = FastAPI(**app_params)

# Setup middleware.
app.add_middleware(
    CORSMiddleware,
    allow_methods=["*"],
    allow_headers=["*"],
    allow_origins=settings.ALLOWED_ORIGINS,
)
add_timing_middleware(app, record=logging.info, prefix="app")

# Include views.
app.include_router(router)

# Set up all services (like database)
setup_services(app)

if __name__ == "__main__":
    uvicorn.run('mavtel.app:app', host='0.0.0.0', port=settings.MAVTEL_WEB_PORT, reload=True)
