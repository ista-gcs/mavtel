from fastapi import APIRouter

from mavtel.api.views.api.metrics import create_metrics_routes
from mavtel.api.views.api.services import create_services_routes
from mavtel.api.views.api.websockets import create_websockets_routes
from mavtel.settings import settings
from mavtel_models.api.api_info import APIInfo

router = APIRouter()


@router.get("/info", response_model=APIInfo)
async def api_info():
    """
    Responds with API info.
    """
    return APIInfo(
        version=settings.VERSION,
        build=settings.BUILD
    )


create_metrics_routes(router)
create_services_routes(router)
create_websockets_routes(router)
