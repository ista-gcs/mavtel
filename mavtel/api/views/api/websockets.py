import logging
import uuid
from typing import Set

from fastapi import APIRouter, WebSocket
from websockets.exceptions import ConnectionClosedError

from mavtel.collector.collector import Collector
from mavtel_models import MAVLinkMetricBaseModel, WSMessageType, MetricsUpdateWSMessage, \
    parse_ws_message

log = logging.getLogger(__name__)

HEARTBEAT_INTERVAL = 1.


def _create_handler(websocket: WebSocket, metrics: Set[str] = None):
    async def __handler(name: str, metric: MAVLinkMetricBaseModel):
        try:
            if metrics is not None and name not in metrics:
                return
            await websocket.send_text(MetricsUpdateWSMessage(
                type=WSMessageType.METRIC_UPDATE,
                name=name,
                data=metric,
            ).json())
        except ConnectionClosedError as _:
            log.debug(f'Failed to send metric "{name}": connection closed.')
        except Exception as send_error:
            log.warning(f'WebSocket error: {send_error}.')

    return __handler


async def _metrics_stream_handler(websocket: WebSocket):
    await websocket.accept()

    connection_id = uuid.uuid4()
    handler_name = f'websocket/{connection_id}'
    collector: Collector = websocket.app.services.collector

    while True:
        try:
            msg = parse_ws_message(await websocket.receive_text())
            if msg.type == WSMessageType.SUBSCRIBE:
                # noinspection PyUnresolvedReferences
                metrics = set(msg.metrics.keys())

                if len(set(collector.get_tracked_metrics()).difference(metrics)) > 0:
                    log.warning(f'Trying to subscribe to not-tracked metrics: '
                                f'{set(collector.get_tracked_metrics()).difference(metrics)}')

                collector.register_handler(
                    name=handler_name,
                    handler=_create_handler(websocket=websocket, metrics=metrics),
                    recreate=True
                )
        except ConnectionClosedError as _:
            break
        except Exception as err:
            log.error(f'WebSocket error: {err}. Aborting connection.')
            break

    collector.deregister_handler(handler_name)
    log.info(f'Closed WevSocket connection: "{connection_id}".')


def create_websockets_routes(router: APIRouter):
    @router.websocket("/streams/metrics")
    async def _all_metrics_stream(websocket: WebSocket):
        await _metrics_stream_handler(websocket=websocket)
