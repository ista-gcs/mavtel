from fastapi import APIRouter, Request

from mavtel.collector.collector import Collector
from mavtel.services.session import Session
from mavtel.storage.metrics_storage import MetricsStorage
from mavtel_models.api.collector_state import CollectorState
from mavtel_models.api.session_state import SessionState
from mavtel_models.api.storage_state import StorageState


def create_services_routes(router: APIRouter):
    @router.get(
        "/services/session",
        name='Session state',
        description='State of MAVTel session',
        response_model=SessionState,
    )
    async def session_state(request: Request):
        session: Session = request.app.services.session
        return SessionState(
            session_id=session.id,
            name=session.name,
            start_utc_timestamp=session.session_start.timestamp(),
        )

    @router.get(
        "/services/collector",
        name='Service state',
        description='State of the MAVLink metric collector service',
        response_model=CollectorState,
    )
    async def collector_state(request: Request):
        collector: Collector = request.app.services.collector
        return CollectorState(
            is_ready=collector.is_ready(),
            is_started=collector.is_started(),
            metrics=collector.get_tracked_metrics(),
            events_processed=collector.get_processed_events_count()
        )

    @router.get(
        "/services/storage",
        name='Storage state',
        description='State of the MAVTel storage service',
        response_model=StorageState,
    )
    async def collector_state(request: Request):
        storage: MetricsStorage = request.app.services.storage
        return StorageState(**storage.state())
