from typing import Optional

import inflection
from fastapi import APIRouter, Request
from fastapi.responses import ORJSONResponse

from mavtel.collector import Collector
from mavtel_models import get_supported_metrics, MAVLinkMetricBaseModel


def _create_metric_route(
        router: APIRouter,
        metric_name: str,
        metric_model_class: MAVLinkMetricBaseModel.__class__
):
    human_metric_name = inflection.humanize(metric_name)

    @router.get(
        f'/metrics/{metric_name}',
        response_model=Optional[metric_model_class],
        name=f'Current {human_metric_name}',
        description=f'Responds with the latest {human_metric_name} ({metric_name}) state.',
        response_class=ORJSONResponse,
    )
    async def _metric(request: Request):
        collector: Collector = request.app.services.collector
        metric: MAVLinkMetricBaseModel = collector[metric_name]

        if metric is None:
            return None

        return metric


def create_metrics_routes(router: APIRouter):
    for metric_name, metric_model_class in get_supported_metrics().items():
        _create_metric_route(
            router=router,
            metric_name=metric_name,
            metric_model_class=metric_model_class
        )
