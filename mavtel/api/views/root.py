from fastapi import APIRouter
from fastapi.responses import RedirectResponse

router = APIRouter()


@router.get("/")
async def info():
    """
    Redirects to the state URL.
    """
    return RedirectResponse("/docs")
