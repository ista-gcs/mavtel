from fastapi import APIRouter

from .views.api.base import router as api_router
from .views.health_checks import router as health_router
from .views.root import router as root_router

# All routes from api module should be included to this router
# which is then included in main.py
router = APIRouter()

router.include_router(root_router, prefix="")
router.include_router(health_router, prefix="/health")
router.include_router(api_router, prefix="/api/v1")
