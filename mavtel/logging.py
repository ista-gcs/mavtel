import logging

from uvicorn.config import LOGGING_CONFIG as UVICORN_LOGGING_CONFIG

from mavtel.settings import settings


class NoUvicornFilter(logging.Filter):
    def filter(self, record: logging.LogRecord):
        return not record.name.startswith('uvicorn')


def setup_logging():
    # noinspection PyUnresolvedReferences
    logging.config.dictConfig(LOGGING)


LOG_LEVEL = settings.LOG_LEVEL.value

# logging dictConfig configuration
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,  # make possible to use other loggers, for example Uvicorg loggers
    'formatters': {
        # see full list of attributes here:
        # https://docs.python.org/3/library/logging.html#logrecord-attributes
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'timestampthread': {
            'format': "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s] [%(name)-20.20s]  %(message)s",
        },
        **UVICORN_LOGGING_CONFIG['formatters'],  # Use Uvicorn formats
    },
    'filters': {
        'nouvicorn': {
            '()': 'mavtel.logging.NoUvicornFilter',
        },
    },
    'handlers': {
        'console': {
            'level': LOG_LEVEL,  # this level or higher goes to the console
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'filters': ['nouvicorn']  # Ignore LogRecord propagated from uvicorn
        },
    },
    'loggers': {
        # root configuration - for all of our own apps
        # (feel free to do separate treatment for e.g. brokenapp vs. sth else)
        'mavtel': {
            'handlers': ['console'],
            'level': LOG_LEVEL,
            'propagate': False,
        },
        '': {
            'handlers': ['console'],
            'level': LOG_LEVEL,
            'propagate': False,
        }

    },
}
