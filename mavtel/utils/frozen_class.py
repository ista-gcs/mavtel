class FrozenClass(object):
    __is_frozen = False

    def __setattr__(self, key, value):
        if self.__is_frozen and not hasattr(self, key):
            raise TypeError(f'Inappropriate attribute "{key}" for class "{self.__class__.__name__}".')
        object.__setattr__(self, key, value)

    def _freeze(self):
        self.__is_frozen = True
