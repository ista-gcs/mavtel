from mavtel.lifecycle.app_services import AppServices
from mavtel.services.session import Session
from mavtel.settings import settings


def setup_session(services: AppServices):
    session = Session(session_id=settings.SESSION_ID)
    services.session = session
