from typing import List, Optional

from fastapi import FastAPI

from mavtel.collector import Collector
from mavtel.mavlink import MAVLinkConnection
from mavtel.services.session import Session
from mavtel.storage.metrics_storage import MetricsStorage
from mavtel.utils.frozen_class import FrozenClass


class AppServices(FrozenClass):
    def __init__(self, app: FastAPI):
        self._app: FastAPI = app
        app.state.services = self

        self._session: Optional[Session] = None
        self._storage: Optional[MetricsStorage] = None
        self._mavlink: Optional[MAVLinkConnection] = None
        self._collector: Optional[Collector] = None

        self._freeze()

    def requires(self, services: List[str]):
        for srv in services:
            if getattr(self, srv) is None:
                raise f'Required service "{srv}" is not present!'

    @property
    def app(self) -> FastAPI:
        return self._app

    @property
    def session(self) -> Session:
        return self._session

    @session.setter
    def session(self, session: Session):
        self._session = session

    @property
    def storage(self) -> MetricsStorage:
        return self._storage

    @storage.setter
    def storage(self, storage: MetricsStorage):
        self._storage = storage

    @property
    def mavlink(self) -> MAVLinkConnection:
        return self._mavlink

    @mavlink.setter
    def mavlink(self, mavlink: MAVLinkConnection):
        self._mavlink = mavlink

    @property
    def collector(self) -> Collector:
        return self._collector

    @collector.setter
    def collector(self, collector: Collector):
        self._collector = collector
