from fastapi import FastAPI

from .app_services import AppServices
from .setup_collector import setup_collector
from .setup_mavlink import setup_mavlink
from .setup_session import setup_session
from .setup_storage import setup_storage


def setup_services(app: FastAPI):
    # Create dependency injection for application services
    app.services = AppServices(app)
    # Define a session
    setup_session(app.services)
    # Setting up MAVLink connection
    setup_mavlink(app.services)
    # Setting up database
    setup_storage(app.services)
    # Setup metrics collector
    setup_collector(app.services)
