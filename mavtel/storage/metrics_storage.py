import logging
from typing import Dict

from mavtel.storage.backends.base import BaseStorageBackend
from mavtel_models.mavlink.base_metric_model import MAVLinkMetricBaseModel

log = logging.getLogger(__name__)


class MetricsStorage:
    def __init__(self, backends: Dict[str, BaseStorageBackend], main_backend: str):
        self._backends = backends
        self._main_backend = main_backend
        self._is_started: bool = False

    def is_started(self) -> bool:
        return self._is_started

    def is_ready(self) -> bool:
        for backend in self._backends.values():
            if not backend.is_ready():
                return False
        return True

    def state(self):
        return dict(
            is_started=self.is_started(),
            is_ready=self.is_ready(),
            backends={name: backend.state() for name, backend in self._backends.items()}
        )

    async def setup(self):
        self._is_started = True

        log.info(f'Main backend is "{self._main_backend}"')

        log.info('Configuring storage backends...')
        for storage_name, storage in self._backends.items():
            await storage.setup()

    async def stop(self):
        log.info('Stopping storage backends...')
        self._is_started = False
        for storage in self._backends.values():
            await storage.stop()

    async def on_metric_update(self, _: str, data_point: MAVLinkMetricBaseModel):
        await self.save_telemetry(data_point)

    async def save_telemetry(self, data_point: MAVLinkMetricBaseModel):
        """
        Saves telemetry for the current session at each backend.
        :param data_point: data point to save
        """
        for storage_name, storage in self._backends.items():
            await storage.save_telemetry(data_point=data_point)
