#!/usr/bin/env bash

set -e

script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
root_dir="$(dirname "${script_dir}")";
mavsdk_version="1.3.0";

pushd "${root_dir}"
echo "Generating requirements from pipenv..."
pipenv requirements > requirements.txt

echo "Fixing MAVSDK version"
sed -i '' "s/-e .\/libs\/MAVSDK-Python/mavsdk==${mavsdk_version}/g" requirements.txt
popd
