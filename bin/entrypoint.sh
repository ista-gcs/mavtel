#!/usr/bin/env bash

set -e

function info-section {
   echo "-----------------------------------------------------------"
   echo -e "$1"
   echo "-----------------------------------------------------------"
}

###############################################################################
# Setup
###############################################################################

script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )"
root_dir="$(dirname "${script_dir}")"
artifacts_dir="${root_dir}/.artifacts"
coverage_dir="${artifacts_dir}/coverage"

task="${1:-server}"
declare -a args=( "${@:2}")

cd "${root_dir}"

###############################################################################
# Tasks
###############################################################################

function start-server() {
  info-section "Starting web application."
  uvicorn mavtel.app:app \
    --host 0.0.0.0 \
    --port "${MAVTEL_WEB_PORT:-8000}" \
    "${args[@]}"
}

function run-tests() {
  info-section "Running tests."
  coverage run --source ./mavtel,./mavtel_models,./mavtel_client -m py.test ./tests
  info-section "Creating test coverage."
  coverage html -d "${coverage_dir}"
}

function run-shell() {
  info-section "Executing shell command: ${args[*]}"
  "${args[@]}"
}

function show-help() {
  echo -e "Usage:
  server [extra arguments]  - starts server
  test                      - runs tests
  shell <shell command>     - executes shell command
  help                      - shows this help"
}

###############################################################################
# Execution
###############################################################################

case "${task}" in
  server)
    start-server
    ;;
  test)
    run-tests
    ;;
  shell)
    run-shell
    ;;
  help)
    show-help
    ;;
  *)
    echo "Unknown task: ${task}"
    show-help
    exit
esac
