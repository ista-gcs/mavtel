MAVTel
======

[MAVLink](https://mavlink.io/en/) telemetry collection based on MAVSDK.

DockerHub repository: [sitin/mavtel](https://hub.docker.com/r/sitin/mavtel).

Python reference client: [mavtel](https://pypi.org/project/mavtel-client/).

Running
-------

To run the MAVTel with dummy backend (see [backends](#backends)):

```shell
docker run --rm --name -p "14540:14540/udp" -p "8000:8000" -e "BACKENDS=dummy" mavlink sitin/mavlink:latest
```

To test this you need to have a MAVLink instance streaming to `localhost:14540` UDP port. The MAVTel API will be
available at [http://localhost:8000/](http://localhost:8000/).

Configuration:

- [MAVLink](#mavlink)
- [Backends](#backends)

Roadmap
-------

- [ ] Add all MAVLink metrics.
- [ ] Create and publish Python client package.
- [ ] Add API for historical data retrieval.
- [ ] Stream metrics via websockets.
- [ ] Create and publish javascript client package.
- [ ] Support more storages:
  - [ ] [Prometheus](https://prometheus.io)
  - [ ] [Apache Druid](https://druid.apache.org)
  - [ ] [Amazon Timestream](https://aws.amazon.com/timestream/)

API
---

Once MAVLink is running, you can visit do page (i.e. [http://localhost:8000/](http://localhost:8000/docs)) to lear about
API. Auto-generated docs are available at [here](docs/api-docs.html).

### WebSockets

> **Note!**
> 
> This is an experimental feature. The protocol is extremely inefficient and will be replaced soon with a lightweight
> MAVLink over websockets (under consideration).

Metric streaming through websockets available at
[ws://localhost:8000/api/v1/streams/metrics](ws://localhost:8000/api/v1/streams/metrics).

To subscribe to metric updates send JSON-encoded message:

```json lines
{
  "type": "SUBSCRIBE",
  "metrics": {
    "<metric_name>": <frequency>,  // Frequency is not supported yet, you can use `null`
    /* ... */
  }
}
```

Metric updates will be sent as:

```json lines
{
  "type": "METRIC_UPDATE",
  "name": "<metric_name>",
  "data": { /* metric data */ }
}
```

You can re-subscribe on the fly for a new metrics set without reconnecting.

MAVLink
-------

MAVlink is configured by `MAVTEL_SYS_ADDR` environment variable.

- `MAVTEL_SYS_ADDR` — MAVLink telemetry connection. Format: `<protocol>://<host>:<port>`. Default: `udp://:14540`.

Note that if you use UDP then you have to publish container port (i.e. `--publish "<host port>:14540/udp"`)

Backends
--------

Currently, MAVTel supports dummy and [InfluxDB](https://docs.influxdata.com/influxdb/v2.3/) backends.

For other storages planned support see [roadmap](#roadmap):

Backend behaviour is determined by session. Set `SESSION_ID` to a fixed value to stream metrix to a predefined 
partition.

You can define backends in `BACKENDS` environment variable. The first listed backend will be considered as the main one.

> **NOTE**
> 
> The main backend will be used in future for historical data manipulations. At the moment there is no specific 
> behaviour for the main backend.

- `SESSION_ID` — session identifier (keep empty for new partition per run).
- `BACKENDS` — comma separated list of backends (e.g. `influxdb,dummy`).

### InfluxDB Backend

Sessions are stored to the buckets `session_<session_id>`.

Data will be buffered (see configuration options below).

 - `INFLUXDB_URL` — URL to InfluxDB (e.g. `http://localhost:8086/`) 
 - `INFLUXDB_ORG` — InfluxDB organization (won't be created).
 - `INFLUXDB_TOKEN` — InfluxDB token.
 - `INFLUXDB_BUFFER_SIZE` — maximum size of the buffer. Set to `0` for no buffering (may lead to performance issues).
 - `INFLUXDB_BUFFER_DELAY_MS` — maximum delay in milliseconds the data will be kept in buffer.

> **WARNING!**
> 
> At the moment the app will crash if InfluxDB instance is unavailable.

### Dummy Backend

This backend does not support historical data. Useful for testing or in the case when only actual data is required.

### Playground Setup

Clone the repository:

```shell
git clone https://gitlab.com/Zyatin/mavtel.git
cd mavtel
```

Initialise environment and pull docker images:

```shell
make init
make pull
```

Set environment variables in created `.env` file. See: [PX4/Gazebo](#playground-px4gazebo),
[InfluxDB](#playground-influxdb).

> **WARNING!**
>
> Environment variables from [`.env.template`](.env.template) are not intended to use in production. For example, we use
> insecure tokens for database connections.

### Playground Usage

You may check the available GNU/Make commands:

```shell
make help
```

Run PX4/Gazebo and InfluxDB:

```shell
make deps
```

Run MAVTel

```shell
make mavtel
```

### Playground: PX4/Gazebo

We use a slightly modified version of
[https://github.com/JonasVautherin/px4-gazebo-headless](https://github.com/JonasVautherin/px4-gazebo-headless) published
at [sitin/px4-gazebo-headless](https://hub.docker.com/r/sitin/px4-gazebo-headless) (supports both AMD64 and ARM64
architectures).

You can run standalone simulator by:

```shell
make px4
```

The simulator will always use the following ports:

- `14550` — main MAVLink port for GCS like [QGroundControl](http://qgroundcontrol.com).
- `14540` — MAVLink telemetry port that will be used by MAVTel.

Check the image [documentation](https://github.com/JonasVautherin/px4-gazebo-headless) for details.

We suggest to always use `typhoon_h480` model and its default world to have access to more metrics and features.

### Playground: InfluxDB

You can run a standalone InfluxDB instance at [http://localhost:8086/]:

```shell
make influxdb
```

Configuration can be set in `.env` file:

 - `DOCKER_INFLUXDB_INIT_MODE` — initialise config (default `setup`).
 - `DOCKER_INFLUXDB_INIT_USERNAME` — admin username
 - `DOCKER_INFLUXDB_INIT_PASSWORD` — admin password (insecure by default) 
 - `DOCKER_INFLUXDB_INIT_ORG` — default organisation (default is `mavtel`), should match 
   [`INFLUXDB_ORG`](#influxdb-backend).
 - `DOCKER_INFLUXDB_INIT_BUCKET` — default bucket (`main` by default).
 - `DOCKER_INFLUXDB_INIT_ADMIN_TOKEN` — admin API token (insecure by default), should match
   [`INFLUXDB_TOKEN`](#influxdb-backend).

> **NOTE!**
> 
> By default, the `.artifacts/influxdb` volume will be mounted for persistence. You can clean it up with:
> 
> ```shell
> make clean-influxdb
> ```

Development
-----------

Prerequisites (tested version provided):

- Docker `>= 20.10.16`.
- Docker Compose `>= 1.29.2` (will be installed during initialisation).
- Pipenv `>= 2022.6.7` (we suggest adding `export PIPENV_VENV_IN_PROJECT=1` to your shell config).

Install requirements:

```shell
make install
```

> **OS X Caveats**
> 
> Since OS X does not have a distribution for MAVSDK-Python, we forced to build it locally. Check the
> [`Pipfile`](Pipfile) and [`requirements.txt`](requirements.txt) before pushing changes.
> 
> Run `make lock` to safely produce valid [`requirements.txt`](requirements.txt) for Docker build. 

Build in Docker:

```shell
make build
```

Run locally

```shell
make mavtel-dev
```
